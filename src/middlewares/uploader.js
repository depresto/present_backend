const tus = require('tus-node-server');
const server = new tus.Server();
server.datastore = new tus.FileStore({
    path: '/public/upload'
});

const uploadApp = express();
uploadApp.all('*', server.handle.bind(server));

server.on(EVENTS.EVENT_UPLOAD_COMPLETE, (event) => {
    console.log(`Upload complete for file ${event.file.id}`);
});

module.exports = uploadApp;
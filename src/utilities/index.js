exports.extractObject = ( obj, keys ) => {
    const returnObj = { };
    keys.forEach( key => { returnObj[ key ] = obj[ key ]; } );

    return returnObj;
};

exports.paginate = ( objCount, pageLength, currentPage ) => {
		let total_pages = Math.ceil(objCount/pageLength);

		return {
      total_pages: total_pages,
      current_page: currentPage,
      has_pre: currentPage > 1 ,
      has_next: currentPage < total_pages
    } 
}

exports.getUploadDir = () => {
    if (process.env.UPLOAD == 'local') {
      return {
        dir: "public/upload/",
        url: "/upload/"
      }
    }
}
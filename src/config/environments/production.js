module.exports = {
    host: "127.0.0.1",
    port: 3000, // change with production port
    logLevel: process.env.LOG_LEVEL,
    secret: process.env.SECRET,
};


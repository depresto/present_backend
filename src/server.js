import "babel-polyfill";
require('dotenv').config();

import express from "express";
import bodyParser from "body-parser";

import config from "./config";
import customResponses from "./middlewares/customResponses";

const app = express( );

const ENV = process.env.NODE_ENV || config.env;

app.set( "env", ENV );

app.use( bodyParser.json( ) );
app.use( customResponses );

app.use( bodyParser.urlencoded({
  extended: true
}));

app.use( express.static('public') );

require( "./app" )( app );

app.use( ( req, res ) => {
    res.notFound( );
} );

app.use( ( err, req, res, next ) => {
		console.error( err.stack )
    next( err );
} );

// Don't remove next !!!!
app.use( ( err, req, res, next ) => { // eslint-disable-line no-unused-vars
    res.status( 503 ).json( {
        success: false,
        error: "server_error",
    } );
} );

module.exports = app;

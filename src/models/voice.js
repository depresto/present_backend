'use strict';
module.exports = (sequelize, DataTypes) => {
  const Voice = sequelize.define('Voice', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    intro: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    thumb_url: DataTypes.STRING,
    author_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Authors",
        key: "id"
      }
    }
  }, {});
  Voice.associate = function(models) {
    // associations can be defined here
    Voice.belongsTo(models.Author, {
      foreignKey: 'author_id'
    })
    Voice.hasMany(models.VoiceFile, {
      foreignKey: 'voice_id'
    });
  };
  return Voice;
};
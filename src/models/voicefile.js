'use strict';
module.exports = (sequelize, DataTypes) => {
  const VoiceFile = sequelize.define('VoiceFile', {
    voice_url: DataTypes.STRING,
    quality: DataTypes.INTEGER,
    voice_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Voices",
        key: "id"
      }
    }
  }, {});
  VoiceFile.associate = function(models) {
    // associations can be defined here
    VoiceFile.belongsTo(models.Voice, {
      foreignKey: 'voice_id'
    })
  };
  return VoiceFile;
};
import authorRouter from "./author/router";
import voiceRouter from "./voice/router";

module.exports = ( app ) => {
    app.use( "/author", authorRouter );
    app.use( "/voice", voiceRouter );
};

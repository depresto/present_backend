const controller = require( "./controller" );

const express = require( "express" );

const router = express.Router( );

// use apiDoc to generate documentation for API routes
// Details on how to use on: http://apidocjs.com/
// install apidoc globally: npm install apidoc -g
// Generate documentation: apidoc -i src/ -o apidoc/

/**
*    @apiGroup Author
*    @api {get} /author Displaying authors list
* 	 @apiVersion 0.1.0
* 	 @apiName ListAuthor
*.
*    @apiParam {Integer} [length=10] 	Optional size of author list
*    @apiParam {Integer} [page=1] 		Optional no. of page in the list
*
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "authors": [
				            {
				                "id": 2,
				                "name": "Test",
				                "intro": "Intro",
				                "thumb_url": "http://localhost:3000/upload/f3222d65f4cb24156612d8ded3a91b13.jpg",
				                "createdAt": "2018-11-15T03:09:10.732Z",
				                "updatedAt": "2018-11-15T03:09:10.732Z"
				            },
				            {
				                "id": 1,
				                "name": "Test",
				                "intro": "Intro",
				                "thumb_url": "http://localhost:3000undefinedd74293630e5dd7d67063db21be2c1c93.jpg",
				                "createdAt": "2018-11-15T03:08:30.489Z",
				                "updatedAt": "2018-11-15T03:08:30.489Z"
				            }
				        ],
				        "pagination": {
				            "total_pages": 1,
				            "current_page": 1,
				            "has_pre": false,
				            "has_next": false
				        }
				    }
				}
*/
router.get( "/", controller.list );

/**
*    @apiGroup Author
*    @api {get} /author/:id 	Displaying author detail
* 	 @apiVersion 0.1.0
* 	 @apiName ShowAuthor
*
*    @apiParam {Integer} id 				Author ID
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "author": {
				            "id": 2,
				            "name": "Test",
				            "intro": "Intro",
				            "thumb_url": "http://localhost:3000undefinedd74293630e5dd7d67063db21be2c1c93.jpg",
				            "createdAt": "2018-11-15T03:08:30.489Z",
				            "updatedAt": "2018-11-15T03:08:30.489Z"
				        }
				    }
				}
*/
router.get( "/:id", controller.detail );


/**
*    @apiGroup Author
*    @api {post} /author 				Create new author
* 	 @apiVersion 0.1.0
* 	 @apiName CreateAuthor
*
*    @apiParam {String} name 		Author's name
*    @apiParam {String} intro 	Author's introduction
*    @apiParam {File} 	thumb 	Author's thumbnail
*
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "author": {
				            "id": 3,
				            "name": "Test",
				            "intro": "Intro",
				            "thumb_url": "http://localhost:3000/upload/f3222d65f4cb24156612d8ded3a91b13.jpg",
				            "updatedAt": "2018-11-15T03:09:10.732Z",
				            "createdAt": "2018-11-15T03:09:10.732Z"
				        }
				    }
				}
*/
router.post( "/", controller.uploadAuthorThumb, controller.create );

export default router;

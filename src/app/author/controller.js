import multer from 'multer';
import path from 'path';
import crypto from 'crypto';

const utilities = require( "../../utilities" );
const repository = require( "./repository" );

const UPLOAD_DIR = utilities.getUploadDir();

exports.create = async ( req, res ) => {
    try {
        const thumb_url = req.protocol + '://' + req.get('host') + UPLOAD_DIR.url + req.file.filename;

        const author = await repository.createAuthor(req.body, thumb_url);
        res.success( {author: author} );
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
}

const storage = multer.diskStorage({
  destination: UPLOAD_DIR.dir,
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)

        cb(null, raw.toString('hex') + path.extname(file.originalname))
    });
  }
});
const upload = multer({ storage: storage })
exports.uploadAuthorThumb = upload.single('thumb');

exports.list = async ( req, res ) => {
    try {
        const length = req.params.length || 10;
        const page = req.params.page || 1;

        const lists = await repository.findAuthors(length, page);

        res.success( {
            authors: lists.rows,
            pagination: utilities.paginate( lists.count, length, page )
        });
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
};

exports.detail = async ( req, res ) => {
    try {
        const details = await repository.findDetails( req.params.id );
        res.success( {author: details} );
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
};

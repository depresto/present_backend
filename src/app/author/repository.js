import { Author } from '../../models';

export const createAuthor = function(data, thumb_url) {
		return Author.create({
			name: data.name,
			intro: data.intro,
            thumb_url: thumb_url
		});
}

export const findAuthors = function(length, page) { 
    return Author.findAndCountAll({
    	order: [
    		['createdAt', 'DESC']
    	],
    	offset: (page - 1) * length,
    	limit: length
    });
}

export const findDetails = function(id) {
    return Author.findById( id );
}

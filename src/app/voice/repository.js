import { Voice } from '../../models';
import { VoiceFile } from '../../models';
import { Author } from '../../models';

export const createVoice = function(data, thumb_url) {
    return Voice.create({
        author_id: data.author_id,
        title: data.title,
        intro: data.intro,
        thumb_url: thumb_url
    });
}

export const createVoiceFile = function(voice, voice_url) {
    return VoiceFile.create({
        voice_id: voice.id,
        voice_url: voice_url
    }, {
        include: [Voice]
    })
}

export const findVoices = function(length, page) { 
    return Voice.findAndCountAll({
        include: [{
            model: VoiceFile,
            required: true
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        offset: (page - 1) * length,
        limit: length
    });
}

export const findDetails = function(id, quality = null) {
    if (quality)
        return Voice.findOne({
            where: {
                id: id,
                quality: quality
            }
        });
    else
        return Voice.findById( id );
}
const controller = require( "./controller" );

const express = require( "express" );

const router = express.Router( );

// use apiDoc to generate documentation for API routes
// Details on how to use on: http://apidocjs.com/
// install apidoc globally: npm install apidoc -g
// Generate documentation: apidoc -i src/ -o apidoc/

/**
*    @apiGroup Voice
*    @api {get} /voice Displaying voices list
* 	 @apiVersion 0.1.0
* 	 @apiName ListVoice
*.
*    @apiParam {Integer} [length=10] 	Optional size of voice list
*    @apiParam {Integer} [page=1] 		Optional no. of page in the list
*
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "authors": [
				            {
				                "id": 2,
				                "name": "Test",
				                "intro": "Intro",
				                "thumb_url": "http://localhost:3000undefinedd74293630e5dd7d67063db21be2c1c93.jpg",
				                "createdAt": "2018-11-15T03:08:30.489Z",
				                "updatedAt": "2018-11-15T03:08:30.489Z"
				            },
				            {
				                "id": 1,
				                "name": "Test",
				                "intro": "Intro",
				                "thumb_url": "http://localhost:3000undefinedd74293630e5dd7d67063db21be2c1c93.jpg",
				                "createdAt": "2018-11-15T03:04:07.885Z",
				                "updatedAt": "2018-11-15T03:04:07.885Z"
				            }
				        ],
				        "pagination": {
				            "total_pages": 1,
				            "current_page": 1,
				            "has_pre": false,
				            "has_next": false
				        }
				    }
				}
*/
router.get( "/", controller.list );

/**
*    @apiGroup Voice
*    @api {get} /voice/:id 	Displaying voice detail
* 	 @apiVersion 0.1.0
* 	 @apiName ShowVoice
*
*    @apiParam {Integer} id 				Voice ID
*    @apiParam {Integer} [quality] 	Optional voice quality (Default: none)
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "id": 2,
				        "title": "Test",
				        "intro": "Intro",
				        "thumb_url": "http://localhost:3000/upload/17e291097398ef82d37101da58f4b385.png",
				        "author_id": 3,
				        "createdAt": "2018-11-15T03:10:51.202Z",
				        "updatedAt": "2018-11-15T03:10:51.202Z"
				    }
				}
*/
router.get( "/:id", controller.detail );

/**
*    @apiGroup Voice
*    @api {post} /voice Create new voice
* 	 @apiVersion 0.1.0
* 	 @apiName CreateVoice

*    @apiParam {String}	title 			Voice's title
*    @apiParam {String} intro 			Voice's introduction
*    @apiParam {File} 	thumb 			Voice thumbnail
*    @apiParam {File} 	voice_file 	Voice file
*
*		 @apiSuccessExample {json} Success-Response:
*				HTTP/1.1 200 OK
				{
				    "success": true,
				    "payload": {
				        "voice": {
				            "id": 2,
				            "author_id": "3",
				            "title": "Test",
				            "intro": "Intro",
				            "thumb_url": "http://localhost:3000/upload/17e291097398ef82d37101da58f4b385.png",
				            "voice_url": "http://localhost:3000/upload/8c4b603c13c47200956b107338e71da5.png"
				        }
				    }
				}
*/
router.post( "/", controller.uploadVoice, controller.create );

export default router;

import multer from 'multer';
import path from 'path';
import crypto from 'crypto';

const utilities = require( "../../utilities" );
const repository = require( "./repository" );

const UPLOAD_DIR = utilities.getUploadDir();

exports.create = async ( req, res ) => {
    try {
        const voice_url = req.protocol + '://' + req.get('host') + UPLOAD_DIR.url + req.files.voice_file[0].filename;
        const thumb_url = req.protocol + '://' + req.get('host') + UPLOAD_DIR.url + req.files.thumb[0].filename;

        const voice = await repository.createVoice(req.body, thumb_url);

        const voicefile = await repository.createVoiceFile(voice, voice_url)

        res.success( {
            voice: {
                id: voice.id,
                author_id: voice.author_id,
                title: voice.title,
                intro: voice.intro,
                thumb_url: voice.thumb_url,
                voice_url: voicefile.voice_url
            }
        } );
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
}

const storage = multer.diskStorage({
  destination: UPLOAD_DIR.dir,
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)

        cb(null, raw.toString('hex') + path.extname(file.originalname))
    });
  }
});
const upload = multer({ storage: storage })
exports.uploadVoice = upload.fields([{
    name: 'thumb'
}, {
    name: 'voice_file'
}]);

exports.list = async ( req, res ) => {
    try {
        const length = req.params.length || 10;
        const page = req.params.page || 1;

        const lists = await repository.findVoices(length, page);
        res.success( {
            authors: lists.rows,
            pagination: utilities.paginate( lists.count, length, page )
        });
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
};

exports.detail = async ( req, res ) => {
    try {
        const details = await repository.findDetails( req.params.id );
        res.success( details );
    } catch ( err ) {
        console.error( err );
        res.serverError( err );
    }
};
